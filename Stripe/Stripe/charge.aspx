﻿<%@ Page Language="C#"  %>
<%@ Import Namespace="System.Threading.Tasks" %>
<%@ Import Namespace="Stripe" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="System.Net.Mail" %>

<!DOCTYPE html>

<script runat="server">
    protected  void Page_Load(object sender, EventArgs e)
    {   
        var chargeId = string.Empty;
        string email = Request.Form["stripeEmail"];       
        try
        {   
            /*Getting tokenid from chekoutpage.*/    
            string tokenId = Request.Form["stripeToken"];
            chargeId =  ChargeCustomer(tokenId);
            bool status = SendMail(email,"mss.vimalsharma@gmail.com","Stripe Payment Sucess","Thank you <br> You have been charged $700.00 for oDesk Stripe.");           
            Response.Redirect("thankyou.html",false);                           
        }
        catch (Exception ex)
        {
            string message = ex.Message;
            bool status = SendMail("mss.vimalsharma@gmail.com", "mss.vimalsharma@gmail.com", "Stripe Error Message", message);             
            Response.Redirect("error.html",false);            
        }
    }
    
    
    //*Charge Customer using tokenid*//
    private string ChargeCustomer(string tokenId)
    {
           var myCharge = new StripeChargeCreateOptions
            {
                Amount = 70000,
                Currency = "usd",
                Description = "Odesk Stripe",
                TokenId = tokenId
            };
            string APIKEY =ConfigurationManager.AppSettings["StripeApiKey"].ToString();
            var chargeService = new StripeChargeService(APIKEY);
            var stripeCharge = chargeService.Create(myCharge);
            return stripeCharge.Id;     
    }
    
    //*Mail Send *//
    public bool SendMail(string To, string from, string subject, string body)
    {
        string host = ConfigurationManager.AppSettings["mandrill_Host"].ToString();
        string port = ConfigurationManager.AppSettings["mandrill_Port"].ToString();
        string username = ConfigurationManager.AppSettings["mandrill_username"].ToString();
        string password = ConfigurationManager.AppSettings["mandrill_password"].ToString();
        bool Result =false ;
        MailMessage mail = new MailMessage();
        mail.To.Add(To);
        mail.From = new MailAddress(from);
        mail.Subject = subject;
        mail.Body = body;
        mail.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        smtp.Host = host;
        smtp.Port = Convert.ToInt32(port);
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = new System.Net.NetworkCredential(from, password);
        smtp.EnableSsl = true;
        try
        {
            smtp.Send(mail);
            Result = true;
        }
        catch (Exception ex)
        {
            Result = false;
        }
        return Result;
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">      
    <div>
    
    </div>
    </form>
</body>
</html>
